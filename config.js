let useLocalhost = true;
let config = null;

if (useLocalhost) {
		config = {janusHost:"ws://192.168.68.245:8188"};
} else {
		config = {janusHost:"wss://y2ding.syndicatetg.com:6503"};
}

export default config;
