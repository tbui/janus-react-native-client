import React, {
    Component
} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    TextInput,
    ListView,
    ScrollView,
    Dimensions,
    Image,
    Alert
} from 'react-native';

import PropTypes from 'prop-types';

import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    RTCVideoView,
    MediaStreamTrack,
    getUserMedia,
} from 'react-native-webrtc';

import Janus from './janus.mobile.js';
import config from '../config.js';
import InCallManager from 'react-native-incall-manager';

import Spinner from 'react-native-loading-spinner-overlay';
import {
    Icon
} from 'react-native-elements'
import TabNavigator from 'react-native-tab-navigator';

import {
    connect
} from 'react-redux';
import {
    bindActionCreators
} from 'redux';

import * as appAction from './action';

let server = config.janusHost;
let janus;
let streaming = null;
let started = false;

const VIDEO = 1;
const WAVEFORMS = 2;

let feeds = [];
var bitrateTimer = [];

Janus.init({
    debug: "all",
    callback: function() {
        if (started)
            return;
        started = true;
    }
});


class Streaming extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => true
        });
        this.state = {
            info: 'Initializing',
            status: 'init',
            selfViewSrc: null,
            selfViewSrcKey: null,
            streams: {},
            streamHandles: {}
        };
    }

    componentDidMount() {
        InCallManager.start({ media: 'video' });
        this.startSession(WAVEFORMS);
    }

    startSession = (index) => {
      var turn = {'url':'turn:y2ding.eastus.cloudapp.azure.com:3478', 'username':'webuser', 'credential':'RedSox2004'};
      var stun = {'url': 'stun:stun.l.google.com:19302'};
      this.setState({
        initialized: true,
        visible: true
      });
      janus = new Janus({
        iceServers: [stun, turn],
        server: server,
        success: () => {
          janus.attach({
            plugin: "janus.plugin.streaming",
            success: function(pluginHandle) {
              streaming = pluginHandle;
              Janus.log("Plugin attached! (" + streaming.getPlugin() + ", id=" + streaming.getId() + ")");
              // start the stream
              var body = {
                "request": "watch",
                id: index
              };
              streaming.send({
                "message": body
              });
            },
            error: (error) => {
              Janus.error("  -- Error attaching plugin... ", error);
              alert("Error attaching plugin... " + error);
            },
            onmessage: (msg, jsep) => {
              Janus.debug(" ::: Got a message :::");
              Janus.debug(msg);
              var result = msg["result"];
              if (result !== null && result !== undefined) {
                if (result["status"] !== undefined && result["status"] !== null) {
                  var status = result["status"];
                  if (status === 'stopped')
                    this.stopSession();
                } else if (msg["streaming"] === "event") {
                  // Is simulcast in place?
                }
              } else if (msg["error"] !== undefined && msg["error"] !== null) {
                Alert.alert(msg["error"]);
                this.stopSession();
                return;
              }
              if (jsep !== undefined && jsep !== null) {
                Janus.debug("Handling SDP as well...");
                Janus.debug(jsep);
                // Offer from the plugin, let's answer
                streaming.createAnswer({
                  jsep: jsep,
                  media: {
                    audioSend: false,
                    videoSend: false
                  }, // We want recvonly audio/video
                  success: function(jsep) {
                    Janus.debug("Got SDP!");
                    Janus.debug(jsep);
                    var body = {
                      "request": "start"
                    };
                    streaming.send({
                      "message": body,
                      "jsep": jsep
                    });
                    //
                  },
                  error: function(error) {
                    Janus.error("WebRTC error:", error);
                    Alert.alert("WebRTC error... " + JSON.stringify(error));
                  }
                });
              }
            },
            onremotestream: (stream) => {
                Janus.debug(" ::: Got a remote stream :::");
                Janus.debug(stream);
                const streams = this.state.streams;
                const streamHandles = this.state.streamHandles;
                streams[index] = stream.toURL();
                streamHandles[index] = streaming;
                // this.setState({ selfViewSrc: stream.toURL() });
                // this.setState({ selfViewSrcKey: Math.floor(Math.random() * 1000) });
                this.setState({ streams: streams, streamHandles: streamHandles });
                this.setState({ visible: false });
                if(index===VIDEO)
                    this.setState({ status : 'streaming-video' });
            },
            oncleanup: () => {
              Janus.log(" ::: Got a cleanup notification :::");
            }
          });
        }
      });
    }

    stopSession = () => {
        var body = {
            "request": "stop"
        };
        streaming.send({
            "message": body
        });
        streaming.hangup();
        janus.destroy();
        this.setState({status: 'ready'});
    }
    render() {
        return ( <ScrollView>
            <View style = {styles.container} > 
            {
                this.state.selfViewSrc && <RTCView key = {this.state.selfViewSrcKey} streamURL = {this.state.selfViewSrc} style = {styles.remoteView} />
            }
            {
                this.state.streams && Object.keys(this.state.streams).map((key, index) => 
                {
                    return <RTCView key={Math.floor(Math.random() * 1000)} streamURL={this.state.streams[key]} style = {styles.remoteView} />
                })
            }
            </View> 
            <View style = {{flex: 1,flexDirection: 'row'}} > 
            { 
                this.state.status === 'streaming-video' ?
                    <Icon
                    raised
                    name='video-off'
                    type='material-community'
                    color='black'
                    onPress={() => this.stopSession()} /> :
                    <Icon
                    raised
                    name = 'video'
                    type = 'material-community'
                    color = 'black'
                    onPress = {() => this.startSession(VIDEO)} /> 
            }
            </View>
            <View style={{ flex: 1 }}>
                <Spinner visible={this.state.visible} textContent={"Connecting..."} textStyle={{color: '#FFF'}} />
            </View>
            </ScrollView>
        );
    }
};

const styles = StyleSheet.create({
    selfView: {
        width: 200,
        height: 150,
    },
    remoteView: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 2.35
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    listViewContainer: {
        height: 150,
    },
});


Streaming.propTypes = {
    App: PropTypes.object.isRequired,
};

function select(store) {
    return {
        App: store.App,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        App: bindActionCreators(appAction, dispatch)
    };
}

export default connect(select, mapDispatchToProps)(Streaming);
